import * as React from "react";
import { Form, DatePicker as DatePickerHelper } from "antd";
import moment from "moment";
export default (props: any): any => {
  const {
    input,
    datePickerProps,
    formItemStyle,
    meta,
    label = null,
    hasFeedback,
    colon = false,
    ...extra
  } = props;
  const hasError = meta.dirty && meta.invalid;
  const { value, onChange, ...filteredInput } = input;
  return (
    <Form.Item
      label={label ? label : null}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error}
      style={{ ...formItemStyle }}
      colon={colon}
    >
      <DatePickerHelper
        className="a8DatePicker"
        value={value ? moment(value) : null}
        onChange={onChange}
        {...filteredInput}
        {...extra}
      />
    </Form.Item>
  );
};
