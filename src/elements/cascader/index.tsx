import * as React from "react";
import {Form, Cascader} from "antd";
export default (props: any) => {
    const {
      input,
      cascaderProps,
      formItemStyle,
      meta,
      label = null,
      hasFeedback
    } = props;
    const hasError = meta.dirty && meta.invalid;
    return (
      <Form.Item
        label={label ? label : null}
        validateStatus={hasError ? "error" : "success"}
        hasFeedback={hasFeedback && hasError}
        help={hasError && meta.error}
        style={{ ...formItemStyle }}
      >
        <Cascader
          value={input.value ? input.value : undefined}
          onChange={input.onChange}
          {...cascaderProps}
        />
      </Form.Item>
    );
  };