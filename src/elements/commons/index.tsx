import * as React from "react";
import {
    Form,
    Input,
    Select as antdSelect,
    Radio as antdRadio,
    Upload,
    Switch as antdSwitch,
    // DatePicker as DatePickerHelper,
    Checkbox as antCheckBox
  } from "antd";
  import "antd/dist/antd.css";

interface ElementProps {
    input: any;
    meta: { touched: boolean; invalid: boolean; error: boolean };
    children: any;
    formItemStyle: object;
    formItemLayout: object;
    label: string;
    hasFeedback: boolean;
    colon: boolean;
  }
    
  export const RenderField = (Component: any) => ({
    input,
    meta,
    children = null,
    formItemStyle = {},
    formItemLayout = {},
    hasFeedback,
    label,
    colon = false,
    ...rest
  }: ElementProps) => {
    const hasError = meta.touched && meta.invalid;
    const { value, onChange, ...filteredInput } = input;
    return (
      <Form.Item
        {...formItemLayout}
        label={label ? label : null}
        validateStatus={hasError ? "error" : "success"}
        hasFeedback={hasFeedback && hasError}
        help={hasError && meta.error}
        style={{ ...formItemStyle }}
        colon={colon}
      >
        <Component
          value={value ? value : undefined}
          onChange={input.onChange}
          {...filteredInput}
          {...rest}
          children={children}
          defaultChecked={Boolean(value)}
        />
      </Form.Item>
    );
  };
  
  
  //connect antd element with redux-form
  export const TextBox = RenderField(Input);
  
  //password field
  export const Password = RenderField(Input.Password);
  
  //conenct antd element with redux-form
  export const TextArea = RenderField(Input.TextArea);
  
  //select component
  export const Select = RenderField(antdSelect);
  
  //select option helper
  export const SelectHelper = antdSelect;
  
  //Radio.Group Wrapper
  export const RadioWrapper = RenderField(antdRadio.Group);
  
  //export radioHelpers
  export const Radio = antdRadio;
  
  //Upload wrapper with redux-form
  export const UploadHelper = RenderField(Upload);
  
  //Switch wrapper
  export const Switch = RenderField(antdSwitch);
  
  //checkbox connector
  export const Checkbox = RenderField(antCheckBox);