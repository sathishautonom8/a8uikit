import React, { Component } from "react";

import {
  Password,
  TextBox,
  TextArea,
  Select,
  SelectHelper,
  DatePicker,
  RadioWrapper,
  Radio,
  Checkbox,
  Switch,
  Uploader
} from "formelements";

import Code from "react-code-prettify";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Tooltip, Icon, Input } from "antd";
// const Option = Select.Option;
const { OptGroup, Option } = SelectHelper;
class App extends Component {
  state = {
    UploaderEsafSample: {
      // name of uploader field name
      fieldName: "UploaderEsafSample",
      /**
       * fileInfo props contain all the fileinfo user need to upload
       * fileInfo.length should be equal to uploadLimit
       * Note : whatever field you specifiy in defaultValuesFieldNames you need include here
       */
      fileInfo: [
        { name: "Adhar Card", key: "AdharCard" },
        { name: "Pan Card", key: "PanCard" },
        { name: "Passport", key: "Passport" }
      ],
      /**
       * defaultValuesFieldNames props responsible for appending default values to uploader
       */
      defaultValuesFieldNames: ["AdharCard"],
      // uploadLimit handle how many fields the user need to upload
      uploadLimit: 3,
      /**
       * errorMsg : handle custom error messages.
       * you can pass to handle different sceneries
       */
      errorMsg: {
        //if upload limit exceed
        uploadLimit: "Upload limit exceed!",
        //if fileInfo.length != uploadLimit below message will show
        fileInfoUploadLimitMisMatch:
          "YOUR FIELD NAME props fileinfo and upload limit should be equal",
        //if multiple file uploads have same name
        variableNameConflict: "File Name should be unique",
        //if file uploaded, force your to select file/variable name
        updateVariableName: "Please Select File Name"
      },
      //this props handle defaultFiles fetching state
      initialUploadLoader: false
    }
  };
  render() {
    return (
      <div style={{ width: "600px", margin: "auto" }}>
        <br />
        {/* Text Box */}
        <Field
          label="TextBox"
          name="text"
          component={TextBox}
          placeholder="TextBox"
          type="text"
          colon={false}
          hasFeedback
          // formItemLayout={{
          //   labelCol : {span : 4},
          //   wrapperCol : {span : 14}
          // }}
        />

        <Field
          label={<span>Password</span>}
          name="password"
          component={Password}
          placeholder="password"
          colon={false}
          hasFeedback
        />

        <Field
          label={
            <span>
              Custom Label&nbsp;
              <Tooltip title="What do you want others to call you?">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          }
          name="text"
          component={TextBox}
          placeholder="Custom Label"
        />

        <Field
          label="TextArea"
          name="TextArea"
          component={TextArea}
          rows={4}
          placeholder="TextArea"
          autosize={{ minRows: 4, maxRows: 16 }} // rows={8}
        />

        <Field
          label="Select"
          name="select"
          component={Select}
          placeholder="Select Placeholder"
          className={"a8Select"}
          // mode="multiple"
        >
          <Option value="1">Option One</Option>
          <Option value="2">Option Two</Option>
          <Option value="3">Option Three</Option>
        </Field>

        <Field
          label="Date Picker"
          name="datepicker"
          component={DatePicker}
          showTime
          placeholder="Select Date"
        />

        <Field
          label="Radio"
          name="radio"
          buttonStyle="soild"
          component={RadioWrapper}
        >
          <Radio.Button value="Yes">Yes</Radio.Button>
          <Radio.Button value="No">No</Radio.Button>
        </Field>

        <Field
          label="Checkbox"
          name="checkbox"
          className="a8CheckBox"
          component={Checkbox}
        />

        <Field
          label="Switch"
          name="switch"
          className="a8Switch"
          component={Switch}
        />

        <Field
          label="Uploader"
          name={this.state.UploaderEsafSample.fieldName}
          component={Uploader}
          multiple={true}
          initialUploadLoader={
            this.state.UploaderEsafSample.initialUploadLoader
          }
          accept=".jpg,.jpeg,.pdf,.png,.docx,.xlsx"
          uploaderConfig={this.state.UploaderEsafSample}
          // validate={[uploadChecker(this.state.UploaderEsafSample)]}
        />
      </div>
    );
  }
}

App = reduxForm({
  form: "signin"
})(App);

const mapStateToProps = state => {
  console.log(state);
  return {
    initialValues: {
      switch: true,
      radio: "Yes",
      checkbox: true
    },
    state: state.user
  };
};

export default connect(
  mapStateToProps,
  {}
)(App);
