# a8uikit.

[![NPM](https://img.shields.io/npm/v/a8uikit.svg)](https://www.npmjs.com/package/a8uikit) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save a8uikit
```

# Usage

#### Currently Supporting Form Elements/Components for Field:

| Component List | Status    |
| -------------- | --------- |
| TextBox        | Done      |
| Password       | Done      |
| TextArea       | Done      |
| Select         | Done      |
| DatePicker     | Done      |
| Radio          | Done      |
| Uploader       | Done      |
| CheckBox       | Done      |
| Switch         | Done      |
| Signature      | `Pending` |

## Input Text/number/password widget example.

`import {TextBox} from "a8uikit";`

```
<Field
 label={"Your Label" || React.Node}
 name={"Field Name"}
 component={TextBox}
 placeholder="placeholder"
 type="text/number/password"
 hasFeedback // optional
 onChange={()=>{}} // optional
/>
```

## Input Password Widget

`import {Password} from "a8uikit";`

```
<Field
 label={"Your Label" || React.Node}
 name={"Field Name"}
 component={Password}
 placeholder="placeholder"
 hasFeedback // optional
/>
```

## TextArea widget example.

`import {TextArea} from "a8uikit";`

```
<Field
label={"Your Label" || React.Node}
name={"Field Name"}
component={TextArea}
rows={4}
placeholder="TextArea"
autosize={{ minRows: 4, maxRows: 16 }} // rows={8}
/>
```

## Select widget example.

`import {Select,SelectHelper} from "a8uikit";`

```
//Extract Option from SelectHelper
const {Option} = SelectHelper

<Field
    label={"Your Label" || React.Node}
    name={"Field Name"}
    component={Select}
    placeholder="Select Placeholder"
    //This class is must
    className={"a8Select"}>
    <Option value="1">Option One</Option>
    <Option value="2">Option Two</Option>
    <Option value="3">Option Three</Option>
 </Field>
```

## DatePickerHelper

`import {DatePicker} from "a8uikit";`

     <Field
         label={"Your Label" || React.Node}
         name={fieldName}
         component={DatePicker}
         showTime //optional
         placeholder="Select Time"
    />

## Radio.

`import {RadioWrapper, Radio} from "a8uikit";`

#### RadioElement support two views, Radio and RadioButtons.

You can change default radio view with radio button like below

```
<Radio.Button value={}>Label</Radio.Button>
```

#### All antdesign radio props configs available for this component.

[Antd design Radio Reference](https://ant.design/components/radio/)

    <Field
     label={"Your Label" || React.Node}
     name="fieldName"
     component={RadioWrapper}
     //optional
     onChange={()=>}
     >
        //Option 1
        <Radio value={1}>A</Radio>
        <Radio value={2} disabled>B</Radio>
        <Radio value={3}>C</Radio>
        <Radio value={4}>D</Radio>
    </Field>

## Checkbox widget example.

`import {Checkbox} from "a8uikit";`

```
<Field
label={"Your Label" || React.Node}
name={"Field Name"}
component={Checkbox}
//class name is must
className="a8CheckBox"
/>
```

## Switch widget example.

`import {Switch} from "a8uikit";`

```
<Field
label={"Your Label" || React.Node}
name={"Field Name"}
component={Switch}
//class name is must
className="a8Switch"
/>
```

## Uploader

### Procedure to follow :

- Import needed files for uploader from helper.
- Create uploader config with field name in state.
- Combine uploder with redux-form field.
- configure uploadChecker field level validation.
- configure retrieveDefaultFiles helper.

##### Step 1: Import needed files for uploader from helper :

```
import {Uploader} from "a8uikit";
import { uploadChecker,retrieveDefaultFiles} from "../../helpers";
```

##### Step 2: Create uploader config with field name in state :

```
  this.state = {
        //the key should be your uploader field name
        UploaderEsaf: {
        // name of uploader field name
        fieldName: "UploaderEsaf",
        /**
         * fileInfo props contain all the fileinfo user need to upload
         * fileInfo.length should be equal to uploadLimit
         * Note : whatever field you specifiy in defaultValuesFieldNames you need include here
         */
        fileInfo: [
          { name: "Adhar Card", key: "AdharCard" },
          { name: "Pan Card", key: "PanCard" },
          { name: "Passport", key: "Passport" }
        ],
        /**
         * defaultValuesFieldNames props responsible for appending default values to uploader
         */
        defaultValuesFieldNames: ["AdharCard"],
        // uploadLimit handle how many fields the user need to upload
        uploadLimit: 3,
        /**
         * errorMsg : handle custom error messages.
         * you can pass to handle different sceneries
         */
        errorMsg: {
          //if upload limit exceed
          uploadLimit: "Test Upload limit exceed!",
          //if fileInfo.length != uploadLimit below message will show
          fileInfoUploadLimitMisMatch:
            "Test UploaderEsaf props fileinfo and upload limit should be equal",
          //if multiple file uploads have same name
          variableNameConflict: "Test File Name should be unique",
          //if file uploaded, force user to select file/variable name
          updateVariableName: "Please Select File Name"
        }
        //this props handle defaultFiles fetching state
        initialUploadLoader: false
      }

  }
```

##### Step 3: Combine uploder with redux-form field. :

`import { Uploader} from "a8uikit";`

```
         <Field
              label="Uploader Helper"
              name="UploaderEsaf"
              //Uploader Component from helper
              component={Uploader}
              //provide what are the format need to support
              accept=".jpg,.jpeg,.pdf,.png,.docx,.xlsx"
              //if you want multiple file selection make multiple={true} or multiple={false}
              multiple={true}
              uploaderConfig={this.state.UploaderEsaf}
              //step 4 configure uploaderChecker validation
              validate={[uploadChecker(this.state.UploaderEsaf)]}>
            </Field>
```

##### Step 4: Configure retrieveDefaultFiles helper (at tabComponent) :

`import { retrieveDefaultFiles} from "../../helpers";`

```
  async componentDidMount() {
    //set initialUploader true
    this.setState(prevState => ({
      UploaderEsafSample: {
       //preveStete.fileUplaoderName
        ...prevState.UploaderEsafSample,
        initialUploadLoader: true
      }
    }));

    //use this helper to retrieve default files
    await retrieveDefaultFiles({
      taskInfo: this.props.taskInfo,
      fileInfo: this.state.UploaderEsafSample,
      fieldPopulator: this.props.fieldPopulator
    });

    //set initialUploadLoader false
    this.setState(prevState => ({
      UploaderEsafSample: {
        //preveStete.fileUplaoderName
        ...prevState.UploaderEsafSample,
        initialUploadLoader: false
      }
    }));
  }
```

## License

MIT © [](https://github.com/)
